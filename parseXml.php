<?php

// Function to transform multi dimensional nested XML document/string to single dimensional key value pairs
function parseNestedXml($xml)
	{
    //Setting result Variable
    $finalArray = [];
    //converting XML to PHP Array
    $xmlStringToArray = simplexml_load_string($xml);
    //converting PHP array to json string
    $jsonString = json_encode($xmlStringToArray);
    //setting what to replace in json string
    $replaceValueArray = ['{', '}', '"', '[', '[', ']'];
    //setting what to replace with
    $replacementValueArray = [',', ',', '', '', '', ''];
    //converting json string to comma delimeted string
    $text = str_replace($replaceValueArray, $replacementValueArray, $jsonString);
    //converting comma delimited string to array
    $stringArray = explode(',', $text);
    //parsing into key value pairs
	foreach($stringArray as $key => $value)
		{
		if (!empty($value))
			{
            //creating key value from string split on :
			$v = explode(':', $value);
			if (isset($v[1]))
				{
				$finalArray[$key . '-' . $v[0]] = $v[1];
				}
			}
		}
   //delivering your result :)
	return $finalArray;
    }

    /*Testing Function Below*/

//Sample Xml String to Parse
$xml = '<?xml version="1.0"?>
<ACORD>
  <SignonRs>
    <Status>
      <StatusCd>0</StatusCd>
      <StatusDesc>Success</StatusDesc>
    </Status>
    <ClientDt>2002-06-16T15:56:00</ClientDt>
    <CustLangPref>US-en</CustLangPref>
    <ClientApp>
      <Org>com.csc</Org>
      <Name>PT</Name>
      <Version>9.0</Version>
    </ClientApp>
    <ServerDt>2003-03-16</ServerDt>
    <Language>US-en</Language>
  </SignonRs>
</ACORD>
';

//Show in Browser
$displayInBrowser = parseNestedXml($xml);
foreach($displayInBrowser as $key => $value)
	{
	echo '<div><strong>' . $key . '</strong>: ' . $value . '</div>';
	}

exit();
?>